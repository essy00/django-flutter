from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect, Http404
from .models import Post
from .forms import PostForm
from django.core.paginator import Paginator


def post_index(request):
    if request.user.is_authenticated:
        post_all = Post.objects.all()
        post_list = []
        for post in post_all:
            if post.confirmed:
                post_list.append(post)
        paginator = Paginator(post_list, 5)

        page_number = request.GET.get('page')
        posts = paginator.get_page(page_number)
        return render(request, 'post/posts.html', {'posts': posts})

    else:
        raise Http404()


def post_detail(request, slug):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, slug=slug)
        if not post.confirmed and not request.user.is_superuser:
            raise Http404()
        context = {
            'post': post
        }

        return render(request, 'post/detail.html', context)

    else:
        raise Http404()


def post_create(request):
    if request.user.is_authenticated:
        form = PostForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            if request.user.is_superuser:
                post.confirmed = True
            post.save()
            return redirect('post:index')

        context = {
            'form': form
        }

        return render(request, 'post/form.html', context)

    else:
        raise Http404()


def post_update(request, slug):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, slug=slug)
        if post.user == request.user and post.confirmed or request.user.is_superuser:
            form = PostForm(request.POST or None, request.FILES or None, instance=post)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post.get_absolute_url())

            context = {
                'form': form,
            }

            return render(request, 'post/form.html', context)
        else:
            raise Http404()
    else:
        raise Http404()


def post_delete(request, slug):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, slug=slug)
        if post.user == request.user and post.confirmed or request.user.is_superuser:
            post.delete()
        else:
            raise Http404()
        return redirect('post:index')

    else:
        raise Http404()


def post_confirm_page(request):
    if request.user.is_superuser:
        post_all = Post.objects.all()
        post_list = []
        for post in post_all:
            if not post.confirmed:
                post_list.append(post)
        paginator = Paginator(post_list, 5)

        page_number = request.GET.get('page')
        posts = paginator.get_page(page_number)
        return render(request, 'post/confirm.html', {'posts': posts})

    else:
        raise Http404()


def post_confirm(request, slug):
    if request.user.is_superuser:
        post = get_object_or_404(Post, slug=slug)
        post.confirmed = True
        post.save()
        return redirect('post:confirm_page')

    else:
        raise Http404()
