from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.filters import SearchFilter, OrderingFilter

from post.models import Post
from post.api.serializers import PostSerializer

from django.shortcuts import render, get_object_or_404


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
def api_detail_post_view(request, slug):
    try:
        post = Post.objects.get(slug=slug)
    except PostSerializer.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if not post.confirmed and not request.user.is_superuser:
        return Response({'response': "You don't have permission to view that."})

    serializer = PostSerializer(post)
    return Response(serializer.data)


@api_view(['PUT', 'POST', ])
@permission_classes((IsAuthenticated, ))
def api_update_post_view(request, slug):
    try:
        post = Post.objects.get(slug=slug)
    except PostSerializer.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user = request.user
    if post.user != user and not request.user.is_superuser or not post.confirmed and not request.user.is_superuser:
        return Response({'response': "You don't have permission to update that."})

    serializer = PostSerializer(post, data=request.data)

    if serializer.is_valid():
        serializer.save()
        data = {'success': 'Confirm successful'}
        return Response(data=data)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE', ])
@permission_classes((IsAuthenticated, ))
def api_delete_post_view(request, slug):
    try:
        post = Post.objects.get(slug=slug)
    except PostSerializer.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user = request.user
    if post.user != user and not request.user.is_superuser or not post.confirmed and not request.user.is_superuser:
        return Response({'response': "You don't have permission to delete that."})

    operation = post.delete()
    data = {}
    if operation:
        data['success'] = 'Delete successful'
    else:
        data['failure'] = 'Delete failed'
    return Response(data=data)


@api_view(['POST', ])
@permission_classes((IsAuthenticated, ))
def api_create_post_view(request):

    user = request.user
    if user.is_superuser:
        post = Post(user=user, confirmed=True)
    else:
        post = Post(user=user)

    serializer = PostSerializer(post, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', ])
@permission_classes((IsAuthenticated, IsAdminUser, ))
def api_confirm_post_view(request, slug):
    try:
        post = Post.objects.get(slug=slug)
    except PostSerializer.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    post.confirmed = True
    post.save()
    data = {'success': 'Confirm successful'}
    return Response(data=data)


@permission_classes((IsAuthenticated, ))
class ApiPostIndexView(ListAPIView):
    queryset = Post.objects.filter(confirmed=True)
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = PageNumberPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('title', 'subtitle',)


@permission_classes((IsAuthenticated, IsAdminUser, ))
class ApiPostConfirmView(ListAPIView):
    queryset = Post.objects.filter(confirmed=False)
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = PageNumberPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('title', 'subtitle',)
