import 'package:flutter_website/globals.dart' as globals;

class Post {

  int id;
  String title;
  String subtitle;
  String content;
  String image;
  bool confirmed;
  String slug;
  String user;

  Post({ this.id, this.title, this.subtitle, this.content, this.image, this.slug, this.user });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      title: json['title'],
      subtitle: json['subtitle'],
      content: json['content'],
      image: 'http://${globals.BASE_IP}:8000${json['image']}',
      slug: json['slug'],
      user: json['username'],
    );
  }

  List fromJsonAll(Map json) {
    List postList = [];
    for(var post in json['results']) {
      postList.add(Post(
        id: post['id'],
        title: post['title'],
        subtitle: post['subtitle'],
        content: post['content'],
        image: post['image'],
        slug: post['slug'],
        user: post['username'],
      ));
    }
    return postList;
  }
}