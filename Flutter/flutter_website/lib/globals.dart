import 'package:flutter_website/post.dart';

//IPv4 Address
const BASE_IP = '';

String API_KEY;
String postDetailLink, postDeleteLink, postUpdateLink, postConfirmLink;
Map apiLinks = {
  'postIndex': 'http://$BASE_IP:8000/api/post/index',
  'postConfirmPage': 'http://$BASE_IP:8000/api/post/confirm',
  'postDetail': 'http://$BASE_IP:8000/api/post/detail',
  'postCreate': 'http://$BASE_IP:8000/api/post/create',
  'postDelete': 'http://$BASE_IP:8000/api/post/delete',
  'postUpdate': 'http://$BASE_IP:8000/api/post/update',

  'logInPage': 'http://$BASE_IP:8000/api/accounts/login',
  'registerPage': 'http://$BASE_IP:8000/api/accounts/register',
};

List posts = [];
Post activePost;

String activeUser;
bool isAdminUser = false;

String nextPage;
bool reloadPage = false;
bool confirmPage = false;

String searchQuery;