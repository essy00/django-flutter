import 'package:flutter/material.dart';

import 'package:flutter_website/pages/postPage.dart';
import 'package:flutter_website/pages/postDetail.dart';
import 'package:flutter_website/pages/postCreate.dart';
import 'package:flutter_website/pages/login.dart';
import 'package:flutter_website/pages/register.dart';
import 'package:flutter_website/pages/searchPage.dart';

void main() => runApp(MaterialApp(
  title: 'Flutter Website',
  initialRoute: LoginPage.routeName,
  routes: {

    PostPage.routeName: (context) => PostPage(),
    PostDetail.routeName: (context) => PostDetail(),
    PostCreate.routeName: (context) => PostCreate(),
    SearchPage.routeName: (context) => SearchPage(),

    LoginPage.routeName: (context) => LoginPage(),
    RegisterPage.routeName: (context) => RegisterPage(),
  },
));
