import 'package:flutter/material.dart';
import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_website/pages/postPage.dart';

import 'package:flutter_website/globals.dart' as globals;


class PostCreate extends StatefulWidget {

  static const routeName = '/create';

  @override
  _PostCreateState createState() => _PostCreateState();
}

class _PostCreateState extends State<PostCreate> {

  bool _isUploading;
  bool _isLoading = false;
  bool imageExists;

  @override
  void initState() {
    super.initState();
    _isUploading = globals.activePost == null ? false : true;
    titleController.text = globals.activePost == null ? '' : globals.activePost.title;
    subtitleController.text = globals.activePost == null ? '' : globals.activePost.subtitle;
    contentController.text = globals.activePost == null ? '' : globals.activePost.content;
    imageExists = globals.activePost == null ? false : true;
    if(imageExists) downloadFile(globals.activePost.image);
  }

  downloadFile(String url) async {
    String imageName = url.split('/').last;
    var response = await http.get(Uri.parse(url));
    String path = (await getTemporaryDirectory()).path;
    image = await File('$path/$imageName').writeAsBytes(response.bodyBytes);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(_isUploading == false ? 'Create Post' : 'Update Post'),
        centerTitle: true,
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
        children: [
          postSection(),
          buttonSection(),
        ],
      )
    );
  }

  createPost(String title, String subtitle, String content, File image, String apiLink) async {
    if(title == '' || subtitle == '' || content == '' || image == null) {
      _isLoading = false;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fill in all the blanks")));
    }
    else {
      String imageName = image.path.split('/').last;
      var dio = Dio(BaseOptions(headers: {HttpHeaders.authorizationHeader: "Token ${globals.API_KEY}"}));

      FormData formData = FormData.fromMap({
        'title': title,
        'subtitle': subtitle,
        'content': content,
        'image': await MultipartFile.fromFile(image.path, filename: imageName),
      });

      Response response = await dio.post(apiLink,
          options: RequestOptions(method: 'POST', followRedirects: false,
              validateStatus: (status) { return status < 500; }),
          data: formData);

      if(response.statusCode == 201) {
        setState(() {
          _isLoading = false;
          globals.reloadPage = true;
          Navigator.pop(context);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Created, waiting for admin's confirmation")));
        });
      }
      else if(response.statusCode == 200) {
        setState(() {
          _isLoading = false;
          globals.reloadPage = true;
          Navigator.popUntil(context, ModalRoute.withName(PostPage.routeName));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Updated, waiting for admin's confirmation")));
        });
      }
      else {
        _isLoading = false;
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Could not be created")));
      }
    }
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: ElevatedButton.icon(
        onPressed: () {
          setState(() {
            _isLoading = true;
          });
          globals.activePost == null ? createPost(titleController.text, subtitleController.text,
          contentController.text, image, globals.apiLinks['postCreate']) : createPost(titleController.text, subtitleController.text,
              contentController.text, image, '${globals.apiLinks['postUpdate']}/${globals.activePost.slug}');
        },
        icon: Icon(Icons.add),
        label: Text(_isUploading == false ? 'Create Post' : 'Update Post', style: TextStyle(color: Colors.white70, fontWeight: FontWeight.bold),),
      ),
    );
  }

  TextEditingController titleController = new TextEditingController();
  TextEditingController subtitleController = new TextEditingController();
  TextEditingController contentController = new TextEditingController();
  PickedFile pickedImage;
  File image;

  Container postSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          txtSection('Title', Icons.title, titleController),
          SizedBox(height: 30.0,),
          txtSection('Subtitle', Icons.subtitles, subtitleController),
          SizedBox(height: 30.0,),
          txtContent('Content', Icons.article_sharp, contentController),
          SizedBox(height: 30.0,),
          _upload(),
        ],
      ),
    );
  }

  TextField txtSection(String title, IconData icon, TextEditingController _controller) {
    return TextField(
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  TextField txtContent(String title, IconData icon, TextEditingController _controller) {
    return TextField(
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  void _choose() async {
    pickedImage = await ImagePicker().getImage(source: ImageSource.gallery);
    image = File(pickedImage.path);
    setState(() {});
  }

  Column _upload() {
    return Column(
      children: [
        ElevatedButton.icon(
          onPressed: () {
            _choose();
          },
          label: image != null ? Text('Uploaded') : Text('Upload Image'),
          icon: Icon(Icons.upload_sharp),
          style: ElevatedButton.styleFrom(
            primary: Colors.green[700],
          ),
        ),
        SizedBox(height: 10,),
        if(image != null) Image.file(image),
        if(image != null) IconButton(
          icon: Icon(Icons.clear, color: Colors.white70,),
          onPressed: () {
            setState(() {
              image = null;
            });
          }
        ),
      ],
    );
  }
}
