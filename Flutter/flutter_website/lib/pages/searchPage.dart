import 'package:flutter/material.dart';

import 'package:flutter_website/globals.dart' as globals;

class SearchPage extends StatefulWidget {

  static const routeName = '/search';

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  TextEditingController _searchQueryController = TextEditingController();

  @override
  void initState() {
    super.initState();
    globals.searchQuery = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: _buildSearchField(),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              if(_searchQueryController.text != '') {
                globals.searchQuery = _searchQueryController.text;
                globals.reloadPage = true;
                Navigator.pop(context);
              }
              else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fill in the blank")));
              }
            },
          )
        ],
        centerTitle: true,
      ),
    );
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQueryController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: "Search",
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white30),
      ),
      style: TextStyle(color: Colors.white, fontSize: 16.0),
    );
  }
}
