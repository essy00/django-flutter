import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';

import 'package:flutter_website/pages/postCreate.dart';

import 'package:flutter_website/post.dart';
import 'package:flutter_website/globals.dart' as globals;


class PostDetail extends StatefulWidget {

  static const routeName = '/detail';

  @override
  _PostDetailState createState() => _PostDetailState();
}

class _PostDetailState extends State<PostDetail> {

  bool confirmed;
  bool _isLoading = false;

  fetchPost(String apiLink) async {
    var jsonData;
    var response = await http.get(
      Uri.parse(apiLink),
      headers: {HttpHeaders.authorizationHeader: "Token ${globals.API_KEY}"},
    );
    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);
      confirmed = jsonData['confirmed'];
      setState(() {
        _isLoading = false;
        globals.activePost = Post.fromJson(jsonData);
      });
    }
    else {
      _isLoading = false;
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Failed to load the post')));
    }
  }

  deletePost(String apiLink) async {
    var response = await http.delete(
      Uri.parse(apiLink),
      headers: {HttpHeaders.authorizationHeader: "Token ${globals.API_KEY}"},
    );

    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Deleted")));
      });
    }
    else {
      _isLoading = false;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Failed to delete')));
    }
  }

  confirmPost(String apiLink) async {
    var response = await http.get(
      Uri.parse(apiLink),
      headers: {HttpHeaders.authorizationHeader: "Token ${globals.API_KEY}"},
    );

    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        Navigator.of(context).pop();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Confirmed")));
      });
    }
    else {
      _isLoading = false;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Failed to confirm')));
    }
  }

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    fetchPost(globals.postDetailLink);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: _isLoading ? Text('Loading') : Text(globals.activePost.title),
        centerTitle: true,
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        children: [
          postDetail(),
        ],
      )
    );
  }

  Widget footerButtons() {
    if(globals.activeUser == globals.activePost.user || globals.isAdminUser) {
      return Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 7,),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.of(context).pushNamed(PostCreate.routeName) .then((value) => setState(() {}));
              },
              icon: Icon(Icons.update),
              label: Text('Update'),
              style: ElevatedButton.styleFrom(
                primary: Colors.green[700],
              ),
            ),
            SizedBox(width: 15,),
            ElevatedButton.icon(
              onPressed: () {
                globals.reloadPage = true;
                globals.postDeleteLink = '${globals.apiLinks['postDelete']}/${globals.activePost.slug}';
                deletePost(globals.postDeleteLink);
              },
              icon: Icon(Icons.delete),
              label: Text('Delete'),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
              ),
            ),
            SizedBox(width: 15,),
            if(!confirmed && globals.isAdminUser) ElevatedButton.icon(
              onPressed: () {
                globals.reloadPage = true;
                globals.postConfirmLink = '${globals.apiLinks['postConfirmPage']}/${globals.activePost.slug}';
                confirmPost(globals.postConfirmLink);
              },
              icon: Icon(Icons.save),
              label: Text('Confirm'),
              style: ElevatedButton.styleFrom(
                primary: Colors.yellowAccent,
              ),
            ),
          ],
        ),
      );
    }
    else {
      return Center();
    }
  }

  Center postDetail() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Card(
            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
            color: Colors.grey[600],
            child: Column(
              children: [
                Image.network(globals.activePost.image),
                SizedBox(height: 5,),
                Text(
                  globals.activePost.title,
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                ),
                SizedBox(height: 5,),
                Text(
                  globals.activePost.subtitle,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                ),
                SizedBox(height: 20,),
                Html(data: globals.activePost.content, style: {
                  "body": Style(
                    fontSize: FontSize(22.0),
                    fontWeight: FontWeight.bold,
                  ),
                },),
                SizedBox(height: 20,),
                Text(
                  'posted by ${globals.activePost.user}',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                ),
                SizedBox(height: 25,),
                footerButtons(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
