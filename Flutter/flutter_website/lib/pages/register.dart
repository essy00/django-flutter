import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter_website/pages/postPage.dart';
import 'package:flutter_website/pages/login.dart';

import 'package:flutter_website/globals.dart' as globals;


class RegisterPage extends StatefulWidget {

  static const routeName = '/register';

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text('Register'),
        centerTitle: true,
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
        children: [
          textSection(),
          buttonSection(),
          loginButton(),
        ],
      ),
    );
  }

  register(String username, String password, String confirmPassword) async {
    Map data = {
      'username': username,
      'password': password,
      'confirm_password': confirmPassword,
    };
    var jsonData;
    var response = await http.post(globals.apiLinks['registerPage'], body: data);
    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);
      if(jsonData['response'] == 'Successfully registered a new user.') {
        setState(() {
          _isLoading = false;
          Navigator.of(context).popAndPushNamed(PostPage.routeName);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Registered")));
          globals.searchQuery = null;
          globals.confirmPage = false;
          globals.activeUser = username;
          globals.isAdminUser = jsonData['isAdminUser'];
          globals.API_KEY = jsonData['token'];
        });
      }
      else {
        _isLoading = false;
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('All the fields must be filled.')));
      }
    }
    else {
      _isLoading = false;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(jsonDecode(response.body)['password'])));
    }
  }

  Center loginButton() {
    return Center(
      child: Column(
        children: [
          SizedBox(height: 20,),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Have an account? ",
                  style: TextStyle(color: Colors.white70, fontSize: 16),
                ),
                TextSpan(
                  text: "Log In",
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () => Navigator.of(context).popAndPushNamed(LoginPage.routeName),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: ElevatedButton(
        onPressed: () {
          setState(() {
            _isLoading = true;
          });
          register(usernameController.text, passwordController.text, confirmPasswordController.text);
        },
        child: Text('Register', style: TextStyle(color: Colors.white70),),
      ),
    );
  }

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          txtUsername('Username', Icons.person, usernameController),
          SizedBox(height: 30.0,),
          txtPassword('Password', Icons.lock, passwordController),
          SizedBox(height: 30.0,),
          txtPassword('Confirm Password', Icons.lock, confirmPasswordController),
        ],
      ),
    );
  }

  TextFormField txtUsername(String title, IconData icon, TextEditingController _controller) {
    return TextFormField(
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  TextFormField txtPassword(String title, IconData icon, TextEditingController _controller) {
    return TextFormField(
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }
}
