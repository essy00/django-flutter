import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:flutter_website/pages/login.dart';
import 'package:flutter_website/pages/postDetail.dart';
import 'package:flutter_website/pages/postCreate.dart';

import 'package:flutter_website/post.dart';
import 'package:flutter_website/globals.dart' as globals;

import 'package:flutter_website/pages/searchPage.dart';


class PostPage extends StatefulWidget {

  static const routeName = '/index';

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  ScrollController _scrollController = new ScrollController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    globals.posts = [];
    globals.activePost = null;
    _isLoading = true;
    fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        setState(() {
          if(globals.nextPage != null) fetchPosts(globals.nextPage);
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[800],
        appBar: AppBar(
          backgroundColor: Colors.grey[700],
          title: GestureDetector(
            onTap: () {
              _scrollController.animateTo(
                0.0,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 300),
              );
            },
            child: Text((globals.searchQuery == null && !globals.confirmPage)? 'Posts': (globals.searchQuery == null && globals.confirmPage)? 'Confirm Posts': 'Searched: ${globals.searchQuery}', style: TextStyle(fontSize: 20, color: Colors.white),),
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(SearchPage.routeName) .then((value) => setState(() {
                  if(globals.reloadPage && globals.searchQuery != null) {
                    globals.posts = [];
                    globals.reloadPage = false;
                    fetchPosts(globals.confirmPage ? '${globals.apiLinks['postConfirmPage']}?search=${globals.searchQuery}' : '${globals.apiLinks['postIndex']}?search=${globals.searchQuery}');
                  }
                }));
              },
            ),
            if(globals.searchQuery != null) IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  setState(() {
                    globals.posts = [];
                    globals.searchQuery = null;
                    fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);
                  });
                }
            )
          ],
          centerTitle: true,
        ),
        floatingActionButton: SpeedDial(
          icon: Icons.settings,
          overlayColor: Colors.black,
          overlayOpacity: 0.5,
          backgroundColor: Colors.grey[400],
          children: [
            SpeedDialChild(
              child: Icon(Icons.logout),
              onTap: () {
                globals.API_KEY = globals.activeUser = globals.isAdminUser = null;
                Navigator.of(context).popAndPushNamed(LoginPage.routeName);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Logged Out")));
              },
              backgroundColor: Colors.red,
            ),
            SpeedDialChild(
                child: Icon(Icons.add),
                onTap: () {
                  globals.activePost = null;
                  Navigator.of(context).pushNamed(PostCreate.routeName) .then((value) => setState(() {
                    if(globals.reloadPage) {
                      globals.posts = [];
                      globals.reloadPage = false;
                      if(globals.searchQuery != null) {
                        fetchPosts(globals.confirmPage ? '${globals.apiLinks['postConfirmPage']}?search=${globals.searchQuery}' : '${globals.apiLinks['postIndex']}?search=${globals.searchQuery}');
                      }
                      else {
                        fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);
                      }
                    }
                  }));
                },
              backgroundColor: Colors.green[700],
            ),
            if(globals.isAdminUser) SpeedDialChild(
              child: globals.confirmPage ? Icon(Icons.pageview) : Icon(Icons.save),
              onTap: () {
                globals.confirmPage = globals.confirmPage ? false : true;
                setState(() {
                  globals.posts = [];
                  globals.searchQuery = null;
                  fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);
                });
              },
              backgroundColor: globals.confirmPage ? Colors.grey : Colors.yellowAccent,
            ),
          ],
        ),
        body: SmartRefresher(
          header: WaterDropHeader(),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
            controller: _scrollController,
            children: [
              postSection(),
            ],
          ),
        )
    );
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      globals.posts = [];
      if(globals.searchQuery != null) {
        fetchPosts(globals.confirmPage ? '${globals.apiLinks['postConfirmPage']}?search=${globals.searchQuery}' : '${globals.apiLinks['postIndex']}?search=${globals.searchQuery}');
      }
      else {
        fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);
      }
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  fetchPosts(String apiLink) async {
    final response = await http.get(
      Uri.parse(apiLink),
      headers: {HttpHeaders.authorizationHeader: "Token ${globals.API_KEY}"},
    );
    if (response.statusCode == 200) {
      jsonDecode(response.body)['next'] != null ? globals.nextPage = jsonDecode(response.body)['next'] : globals.nextPage = null;

      setState(() {
        _isLoading = false;
      });
      globals.posts = [...globals.posts, ...Post().fromJsonAll(jsonDecode(response.body))];
    }
    else {
      _isLoading = false;
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Failed to load the post list')));
    }
  }

  Widget postSection() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: globals.posts.map((post) =>
            GestureDetector(
              onTap: () {
                globals.postDetailLink =
                '${globals.apiLinks['postDetail']}/${post.slug}';
                Navigator.pushNamed(context, PostDetail.routeName).then((
                    value) => setState(() {
                  if(globals.reloadPage) {
                    globals.posts = [];
                    globals.reloadPage = false;
                    if(globals.searchQuery != null) {
                      fetchPosts(globals.confirmPage ? '${globals.apiLinks['postConfirmPage']}?search=${globals.searchQuery}' : '${globals.apiLinks['postIndex']}?search=${globals.searchQuery}');
                    }
                    else {
                      fetchPosts(globals.confirmPage ? globals.apiLinks['postConfirmPage'] : globals.apiLinks['postIndex']);
                    }
                  }
                }));
              },
              child: Card(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                color: Colors.grey[700],
                child: Column(
                  children: [
                    Image.network(post.image),
                    SizedBox(height: 10,),
                    Text(
                      '${post.title}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      post.subtitle,
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                    SizedBox(height: 15,),
                  ],
                ),
              ),
            )).toList(),
      ),
    );
  }
}
