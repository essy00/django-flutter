import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter_website/pages/postPage.dart';
import 'package:flutter_website/pages/register.dart';

import 'package:flutter_website/globals.dart' as globals;


class LoginPage extends StatefulWidget {

  static const routeName = '/login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text('Log In'),
        centerTitle: true,
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
        children: [
          textSection(),
          buttonSection(),
          registerButton(),
        ],
      ),
    );
  }

  logIn(String username, String password) async {
    Map data = {
      'username': username,
      'password': password,
    };
    var jsonData;
    var response = await http.post(globals.apiLinks['logInPage'], body: data);
    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);
      if(jsonData['response'] == 'Successfully authenticated.') {
        setState(() {
          _isLoading = false;
          Navigator.of(context).popAndPushNamed(PostPage.routeName);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Logged In")));
          globals.searchQuery = null;
          globals.confirmPage = false;
          globals.activeUser = username;
          globals.isAdminUser = jsonData['isAdminUser'];
          globals.API_KEY = jsonData['token'];
        });
      }
      else {
        _isLoading = false;
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Invalid Credentials')));
      }
    }
    else {
      _isLoading = false;
      setState(() {});
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Failed to log in")));
    }
  }

  Center registerButton() {
    return Center(
      child: Column(
        children: [
          SizedBox(height: 20,),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Don't have an account? ",
                  style: TextStyle(color: Colors.white70, fontSize: 16),
                ),
                TextSpan(
                  text: "Register",
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () => Navigator.of(context).popAndPushNamed(RegisterPage.routeName),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: ElevatedButton(
        onPressed: () {
          setState(() {
            _isLoading = true;
          });
          logIn(usernameController.text, passwordController.text);
        },
        child: Text('Log In', style: TextStyle(color: Colors.white70),),
      ),
    );
  }

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          txtUsername('Username', Icons.person, usernameController),
          SizedBox(height: 30.0,),
          txtPassword('Password', Icons.lock, passwordController),
        ],
      ),
    );
  }

  TextFormField txtUsername(String title, IconData icon, TextEditingController _controller) {
    return TextFormField(
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  TextFormField txtPassword(String title, IconData icon, TextEditingController _controller) {
    return TextFormField(
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
      controller: _controller,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        suffixIcon: IconButton(
          onPressed: () => _controller.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }
}
